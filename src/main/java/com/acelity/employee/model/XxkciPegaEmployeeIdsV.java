package com.acelity.employee.model;

import java.io.Serializable;

import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Immutable;

@Entity
@Immutable
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@NamedQueries({ @NamedQuery(name = "XxkciPegaEmployeeIdsV.findAll", query = "select o from XxkciPegaEmployeeIdsV o") })
@Table(name = "APPS.XXKCI_PEGA_EMPLOYEE_IDS_V")
@XmlRootElement(name = "XxkciPegaEmployeeIdsV")
@XmlAccessorType (XmlAccessType.FIELD)
public class XxkciPegaEmployeeIdsV implements Serializable {
    private static final long serialVersionUID = -9127183600223135857L;
    @Id
   	@Column(name = "ID",  updatable =false)
    public String ID;
    @Temporal(TemporalType.DATE)
    @Column(name = "ACTUAL_TERMINATION_DATE")
    private Date actualTerminationDate;
    @Column(name = "EMP_TITLE_CODE", length = 60)
    private String empTitleCode;
    @Column(name = "EMP_TITLE_DESCRIPTION", length = 240)
    private String empTitleDescription;
    @Column(name = "EMPLOYEE_NUMBER", length = 30)
    private String employeeNumber;
    @Column(name = "FIRST_NAME", length = 150)
    private String firstName;
   
    @Column(name = "LAST_NAME", length = 150)
    private String lastName;
    @Column(length = 60)
    private String location;
    @Temporal(TemporalType.DATE)
    @Column(name = "PERSON_EFF_START_DATE")
    private Date personEffStartDate;
    @Column(name = "PERSON_ID")
    private Long personId;
    @Column(name = "PERSON_OBJECT_VERSION_NUMBER")
    private Integer personObjectVersionNumber;
    @Column(name = "PERSON_TYPE", length = 4000)
    private String personType;
    @Column(name = "PERSON_TYPE_ID")
    private Long personTypeId;

    public XxkciPegaEmployeeIdsV() {
    }

    public XxkciPegaEmployeeIdsV(Date actualTerminationDate, String empTitleCode, String empTitleDescription,
                                 String employeeNumber, String firstName, String lastName, String location,
                                 Date personEffStartDate, Long personId, Integer personObjectVersionNumber,
                                 String personType, Long personTypeId) {
        this.actualTerminationDate = actualTerminationDate;
        this.empTitleCode = empTitleCode;
        this.empTitleDescription = empTitleDescription;
        this.employeeNumber = employeeNumber;
        this.firstName = firstName;
        this.lastName = lastName;
        this.location = location;
        this.personEffStartDate = personEffStartDate;
        this.personId = personId;
        this.personObjectVersionNumber = personObjectVersionNumber;
        this.personType = personType;
        this.personTypeId = personTypeId;
    }

    public Date getActualTerminationDate() {
        return actualTerminationDate;
    }

    public void setActualTerminationDate(Date actualTerminationDate) {
        this.actualTerminationDate = actualTerminationDate;
    }

    public String getEmpTitleCode() {
        return empTitleCode;
    }

    public void setEmpTitleCode(String empTitleCode) {
        this.empTitleCode = empTitleCode;
    }

    public String getEmpTitleDescription() {
        return empTitleDescription;
    }

    public void setEmpTitleDescription(String empTitleDescription) {
        this.empTitleDescription = empTitleDescription;
    }

    public String getEmployeeNumber() {
        return employeeNumber;
    }

    public void setEmployeeNumber(String employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getId() {
        return ID;
    }

    public void setId(String id) {
        this.ID = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Date getPersonEffStartDate() {
        return personEffStartDate;
    }

    public void setPersonEffStartDate(Date personEffStartDate) {
        this.personEffStartDate = personEffStartDate;
    }

    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    public Integer getPersonObjectVersionNumber() {
        return personObjectVersionNumber;
    }

    public void setPersonObjectVersionNumber(Integer personObjectVersionNumber) {
        this.personObjectVersionNumber = personObjectVersionNumber;
    }

    public String getPersonType() {
        return personType;
    }

    public void setPersonType(String personType) {
        this.personType = personType;
    }

    public Long getPersonTypeId() {
        return personTypeId;
    }

    public void setPersonTypeId(Long personTypeId) {
        this.personTypeId = personTypeId;
    }
}
