package com.acelity.employee.ws;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;

 
import com.acelity.employee.dao.EmployeeDAO;
import com.acelity.employee.model.XxkciPegaEmployeeIdsV;

@javax.jws.WebService(name = "EmployeeWS", serviceName = "EmployeeWS", targetNamespace = "http://com.kci.cache/employeeCache")

public class EmployeeWS 
{
	private EmployeeDAO employeeDAO;
	
	@WebMethod(exclude = true)
	public void setEmployeeDAO(EmployeeDAO employeeDAO) {
		this.employeeDAO = employeeDAO;
	}
	@WebMethod(operationName = "getEmployee")
	public List<XxkciPegaEmployeeIdsV> getEmployee(
			@WebParam(name = "employeeNumber") String employeeNumber,
			@WebParam(name = "firstName") String firstName, 
			@WebParam(name = "lastName") String lastName,
			@WebParam(name = "personType") String personType, 
			@WebParam(name = "location") String location,
			@WebParam(name = "empTitleDescription") String empTitleDescription) {
		return employeeDAO.getEmployee(employeeNumber, firstName, lastName, personType, location,
				empTitleDescription);
	}
}
