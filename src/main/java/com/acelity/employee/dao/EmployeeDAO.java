package com.acelity.employee.dao;

import java.util.List;

import com.acelity.employee.model.XxkciPegaEmployeeIdsV;

public interface EmployeeDAO {

	List<XxkciPegaEmployeeIdsV> getEmployee(String employeeNumber, String firstName, String lastName, String personType, String location, String empTitleDescription);
	
}
