package com.acelity.employee.dao;

import javax.xml.ws.Endpoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.acelity.employee.ws.EmployeeWS;
public class EmployeeRun {
	private static Logger logger = null;  
    public static void main(String[] args) {
    	logger = LoggerFactory.getLogger(EmployeeRun.class);
        ApplicationContext ctx = new ClassPathXmlApplicationContext("spring-core.xml");
        EmployeeWS service = (EmployeeWS) ctx.getBean("serviceBean");

        Endpoint.publish("http://0.0.0.0:7002/employeeCache", service);
        logger.info("Server start in Port .. 7002");
    }
}
