package com.acelity.employee.dao.impl;

import java.util.Collections;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

 
import com.acelity.employee.dao.EmployeeDAO;
import com.acelity.employee.model.XxkciPegaEmployeeIdsV;

public class EmployeeDAOImpl implements EmployeeDAO {

	private SessionFactory sessionFactory;
	public void setSessionFactory(SessionFactory sessionFactory) 
	{
	    this.sessionFactory = sessionFactory;
	}
	public SessionFactory  getSessionFactory() {
		return this.sessionFactory;
	}

	public List<XxkciPegaEmployeeIdsV> getEmployeeWithCriteria(String employeeNumber, String firstName, String lastName,
			String personType, String location, String empTitleDescription) {
		Session session = sessionFactory.openSession();

		Criteria crit = session.createCriteria(XxkciPegaEmployeeIdsV.class);
		if (employeeNumber != null)
			crit.add(Restrictions.ilike("employeeNumber", employeeNumber));

		if (firstName != null && !firstName.isEmpty())
			crit.add(Restrictions.ilike("firstName", firstName + "%", MatchMode.ANYWHERE));

		if (lastName != null && !lastName.isEmpty())
			crit.add(Restrictions.ilike("lastName", lastName + "%", MatchMode.ANYWHERE));

		if (personType != null && !personType.isEmpty())
			crit.add(Restrictions.ilike("personType", personType + "%", MatchMode.ANYWHERE));

		if (location != null && !location.isEmpty())
			crit.add(Restrictions.ilike("location", location + "%", MatchMode.ANYWHERE));

		if (empTitleDescription != null && !empTitleDescription.isEmpty())
			crit.add(Restrictions.ilike("empTitleDescription", empTitleDescription + "%", MatchMode.ANYWHERE)); 

		crit.addOrder(Order.asc("employeeNumber")).addOrder(Order.asc("firstName")).addOrder(Order.asc("lastName"));

		List<XxkciPegaEmployeeIdsV> employees = crit.list();
		session.close();
		if (employees != null && !employees.isEmpty()) {
			return employees;
		}
		return Collections.emptyList();
	}
	

	public List<XxkciPegaEmployeeIdsV> getEmployee(String employeeNumber, String firstName, String lastName,
			String personType, String location, String empTitleDescription) {
		 Session session = sessionFactory.openSession();
	       
	        
	        StringBuffer hql=new StringBuffer("from XxkciPegaEmployeeIdsV o where 1=1 ");
		      
	        if(employeeNumber!=null && !employeeNumber.isEmpty())
	        {
	        	hql.append(" and employeeNumber = :employeeNumber");
	        }
	        
	        
	        if(firstName!=null && !firstName.isEmpty())
	        {
	        	hql.append(" and firstName like :firstName");
	        }
	        
	        
	        if(lastName!=null && !lastName.isEmpty())
	        {
	        	hql.append(" and lastName like :lastName");
	        }
	      
	        
	        if(personType!=null && !personType.isEmpty())
	        {
	        	hql.append(" and personType like :personType");
	        }
	        
	        
	        if(location!=null && !location.isEmpty())
	        {
	        	hql.append(" and location like :location");
	        }
	       
	        
	        if(empTitleDescription!=null && !empTitleDescription.isEmpty())
	        {
	        	hql.append(" and empTitleDescription like :empTitleDescription");
	        }
	      
	          
	        hql.append("  order by employeeNumber, firstName, lastName");
	        
	        Query query = session.createQuery(hql.toString());
	        
	        if(employeeNumber!=null && !employeeNumber.isEmpty())
	        {
	        	query.setParameter("employeeNumber",employeeNumber);
	        }
	        
	        
	        if(firstName!=null && !firstName.isEmpty())
	        {
	        	query.setParameter("firstName",firstName+"%");
	        }
	        
	        
	        if(lastName!=null && !lastName.isEmpty())
	        {
	        	query.setParameter("lastName",lastName+"%");
	        }
	      
	        
	        if(personType!=null && !personType.isEmpty())
	        {
	        	query.setParameter("personType",personType+"%");
	        }
	        
	        
	        if(location!=null && !location.isEmpty())
	        {
	        	query.setParameter("location",location+"%");
	        }
	       
	        
	        if(empTitleDescription!=null && !empTitleDescription.isEmpty())
	        {
	        	query.setParameter("empTitleDescription",empTitleDescription+"%");
	        }
	        
		              
	        List<XxkciPegaEmployeeIdsV> pg =  query.list();
	        session.close();
	        if(pg != null && !pg.isEmpty())
	        {
	            return pg;
	        }
	        return Collections.emptyList();
	}

}
